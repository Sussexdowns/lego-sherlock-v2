﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchRotateGameControl : MonoBehaviour
{
    
    [SerializeField]
    private Transform[] pictures;

    [SerializeField]
    private GameObject winText;

    public static bool youWin;

    void Start (){
        winText.SetActive(false);
        youWin = false;
    }

    void Update () {



        if (

            (pictures[0].rotation.z == 0 || pictures[0].rotation.z == 360) &&
            (pictures[1].rotation.z == 0 || pictures[1].rotation.z == 360) &&
            (pictures[2].rotation.z == 0 || pictures[2].rotation.z == 360) &&
            (pictures[3].rotation.z == 0 || pictures[3].rotation.z == 360) &&
            (pictures[4].rotation.z == 0 || pictures[4].rotation.z == 360) &&
            (pictures[5].rotation.z == 0 || pictures[5].rotation.z == 360) &&
            (pictures[6].rotation.z == 0 || pictures[6].rotation.z == 360) &&
            (pictures[7].rotation.z == 0 || pictures[7].rotation.z == 360) &&
            (pictures[8].rotation.z == 0 || pictures[8].rotation.z == 360) &&
            (pictures[9].rotation.z == 0 || pictures[9].rotation.z == 360) &&
            (pictures[10].rotation.z == 0 || pictures[10].rotation.z == 360) &&
            (pictures[11].rotation.z == 0 || pictures[11].rotation.z == 360) &&
            (pictures[12].rotation.z == 0 || pictures[12].rotation.z == 360) &&
            (pictures[13].rotation.z == 0 || pictures[13].rotation.z == 360) &&
            (pictures[14].rotation.z == 0 || pictures[14].rotation.z == 360) &&
            (pictures[15].rotation.z == 0 || pictures[15].rotation.z == 360) &&
            (pictures[16].rotation.z == 0 || pictures[16].rotation.z == 360) &&
            (pictures[17].rotation.z == 0 || pictures[17].rotation.z == 360) &&
            (pictures[18].rotation.z == 0 || pictures[18].rotation.z == 360) &&
            (pictures[19].rotation.z == 0 || pictures[19].rotation.z == 360) &&
            (pictures[20].rotation.z == 0 || pictures[20].rotation.z == 360) &&
            (pictures[21].rotation.z == 0 || pictures[21].rotation.z == 360) &&
            (pictures[22].rotation.z == 0 || pictures[22].rotation.z == 360) &&
            (pictures[23].rotation.z == 0 || pictures[23].rotation.z == 360)
            )
            {
                youWin = true;
                winText.SetActive(true);
            }
    }
}
