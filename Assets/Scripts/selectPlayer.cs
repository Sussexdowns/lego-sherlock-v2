﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;



public class selectPlayer : MonoBehaviour
{
    
    private GameObject[] characterList;
    private int index;

    public GameObject characterModels;

    private GameObject tPlayer;
    private Transform tFollowTarget;
    //private CinemachineVirtualCamera vcam;
    private CinemachineFreeLook vcam;


    // Start is called before the first frame update
    void Start()
    {
        index = PlayerPrefs.GetInt("MiniFig");
        Debug.Log("current:"+ index);
    


        foreach (Transform character in characterModels.transform)
        {
            character.gameObject.SetActive(false);
        }

        //vcam = GetComponent<CinemachineVirtualCamera>();
        vcam = GetComponent<CinemachineFreeLook>();

        if (tPlayer == null)
            {
                tPlayer = characterModels.transform.GetChild(index).gameObject;

                if (tPlayer != null)
                {
                    // We toogle on the selected character
                    tPlayer.SetActive(true);
                    tFollowTarget = tPlayer.transform;
                    vcam.LookAt = tFollowTarget;
                    vcam.Follow = tFollowTarget;
                }
            }
        }

    }

   