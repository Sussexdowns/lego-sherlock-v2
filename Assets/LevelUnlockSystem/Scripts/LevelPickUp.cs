﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LevelUnlockSystem
{
    public class LevelPickUp : MonoBehaviour
    {

        [SerializeField] private GameObject overPanel;
        [SerializeField] private GameObject win;
        [SerializeField] private GameObject player;
        public int Distance = 0;
        public int Stars = 0;
        public GameUI gameUI;


        private bool visited = false;

        void Start()
        {
            //gameUI = new GameUI();
        }


        void Update()
        {
            if (win != null && player != null && visited == false)
            {
                float dist = Vector3.Distance(win.transform.position, player.transform.position);

                if (dist < Distance)
                {
                    overPanel.SetActive(true);
                    //Debug.Log("stars"+ Stars);

                    gameUI.GameOver(Stars);


                }
            }
        }

        //method called by close button
        public void CloseBtn()
        {

            visited = true;
            overPanel.SetActive(false);
            
        }

    }

    
}
