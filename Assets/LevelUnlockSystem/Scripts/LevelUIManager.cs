﻿using UnityEngine;

/// <summary>
/// This script create the grid of level buttons in LevelMenu
/// </summary>
namespace LevelUnlockSystem
{
    public class LevelUIManager : MonoBehaviour
    {
        private static LevelUIManager instance;                             //instance variable
        public static LevelUIManager Instance { get => instance; }          //instance getter

        [SerializeField] private LevelButtonScript levelBtnPrefab;              //ref to LevelButton prefab
        [SerializeField] private Transform levelBtnGridHolder;                  //ref to grid holder

        private void Start()
        {
            InitializeUI();
        }

        private void Awake()
        {
            //if instance is null
            if (instance == null)                                               
            {
                //set this as instance
                instance = this;                                                
            }
            //else destroy it
            else
            {
                Destroy(gameObject);                                            
            }
        }

        //method to create the level buttons
        public void InitializeUI()                                             
        {
            //get the level data array
            LevelItem[] levelItemsArray = LevelSystemManager.Instance.LevelData.levelItemArray;  
            //loop through entire array
            for (int i = 0; i < levelItemsArray.Length; i++)                         
            {
                //create button for each element in array
                LevelButtonScript levelButton = Instantiate(levelBtnPrefab, levelBtnGridHolder);
                
                //and set the button
                levelButton.SetLevelButton(levelItemsArray[i], i, i == LevelSystemManager.Instance.LevelData.lastUnlockedLevel);                      
            }
        }
    }
}