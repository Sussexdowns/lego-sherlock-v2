﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace LevelUnlockSystem
{
    /// <summary>
    /// This is example script to show how to use the LevelSystemManager
    /// </summary>
    public class GameUI : MonoBehaviour
    {
        [SerializeField] private Image[] starsArray;
        //array of stars
        [SerializeField] private Text levelStatusText;
        //level status text
        [SerializeField] private GameObject overPanel;
        //ref to over panel
        [SerializeField] private Color lockColor, unlockColor;
        //ref to colors

        //method called by the buttons in the scene
        public void GameOver(int starCount)                     
        {
            //if start count is more than 0
            if (starCount > 0)                                  
            {
                //set the levelStatusText
                levelStatusText.text = "Level " + (LevelSystemManager.Instance.CurrentLevel + 1) + " Complete";
                //send the information to LevelSystemManager
                LevelSystemManager.Instance.LevelComplete(starCount);

            }
            else
            {
                //else only set the levelStatusText
                levelStatusText.text = "Level " + (LevelSystemManager.Instance.CurrentLevel + 1) + " Failed";
            }

            //set the stars
            SetStar(starCount);
            //activate the over panel
            overPanel.SetActive(true); 
        }

        //method called by ok button
        public void OkBtn()                                     
        {

            //overPanel.SetActive(false);
            //SceneManager.LoadScene("Menu Unlock");
        }

        

        /// <summary>
        /// Method to show number of stars achieved by the player for this perticular level
        /// </summary>
        /// <param name="starAchieved"></param>
        private void SetStar(int starAchieved)
        {
            //loop through entire star array
            for (int i = 0; i < starsArray.Length; i++)             
            {
                /// <summary>
                /// if i is less than starAchieved
                /// Eg: if 2 stars are achieved we set the start at index 0 and 1 color to unlockColor, as array start from 0 element
                /// </summary>
                if (i < starAchieved)
                {
                    //set its color to unlockColor
                    starsArray[i].color = unlockColor;              
                }
                else
                {
                    //else set its color to lockColor
                    starsArray[i].color = lockColor;                
                }
            }
        }
    }
}